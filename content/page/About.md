---
title: "C'est Moi!"
---

Hello! I’m Richie Lee. I do computer stuff. This site is about that stuff and some of the stuff I have learned.

When I’m not learning more about computers I can be found running either to or from home (a place called Carshalton in Surrey, England), where I live with my wife Syreeta and my kids Phoebe, Benny and Lucy.

Currently I am a senior consultant at [sabin.io](sabin.io). It’s a data engineering consultancy based in the UK. It’s a lot more fun than it sounds. That’s not sarcasm: I’m honestly not doing it justice, I’m just going for brevity.

I’ve got some projects in GitHub. Some are as done as they’ll ever be (TurboLogShip) some are demo databases (CobaltDb and Canard), some are projects I’ve resurrected from other peoples work and have passed on to others (SSAS Activity Monitor). Over on the sabinio page there's a tonne of stuff I've done, including automating SQL Agent Job Deployment (saLt) and automating ispacs and configuring the environments for hte dsx packages to use (AssistDeploy) I’ve also contributed to dbatools: if you've ever deployed a dacpac using dbatools then you now know who to blame for all your failing deployments ;-)

Rotom Image in my header courtesy of the hard work that [ToppeHatte](https://twitter.com/ToppeHatte) put into some excellent wallpapers.

[![Build Status](https://gitlab.com/RichieBzzzt/source.bzzztio/badges/master/build.svg)](https://gitlab.com/RichieBzzzt/source.bzzztio/commits/master)
